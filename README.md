This document provides an overview of the Continuous Integration and Continuous Deployment (CI/CD) process for the project hosted on GitLab. 

The project consists of multiple repositories:-

Application Repository: https://gitlab.com/devops8834037/assignmentapp

Contains the source code for the application including the .gitlab-ci.yml, Dockerfile, and index.js

Terraform Repository: https://gitlab.com/devops8834037/assignmentterraform/

Contains Terraform configuration files for provisioning infrastructure on AWS.

CI/CD Pipeline Overview
The CI/CD pipeline is divided into three stages: terraform, build, and deploy. Each stage performs specific tasks as described below:

1. Terraform Stage
Purpose: Provision infrastructure on AWS using Terraform.
Steps:
Install Terraform.
Clone the Terraform repository.
Initialize Terraform and perform a plan.
Apply the Terraform configuration to provision infrastructure.

2. Build Stage
Purpose: Build the Docker image of the application.
Steps:
Use Docker to build the application image.
Log in to Docker Hub.
Push the built Docker image to Docker Hub.

3. Deploy Stage
Purpose: Deploy the application on AWS EC2 instances.
Steps:
Install AWS CLI and configure access credentials.
Get instance IDs of EC2 instances.
Send commands to EC2 instances via AWS Systems Manager (SSM) to pull the latest Docker image and run the container.

